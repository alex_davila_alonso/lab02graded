/* Alex Davila Alonso
 * 2132425
 */

package application;

import vehicles.Bicycle;

public class BikeStore {
    public static void main(String args[]) {
        Bicycle[] bikes = new Bicycle[4];
        bikes[0] = new Bicycle("Specialized", 21, 50);
        bikes[1] = new Bicycle("Giant", 23, 100);
        bikes[2] = new Bicycle("Giant", 23, 80);
        bikes[3] = new Bicycle("Specialized", 21, 65);

        for (Bicycle newBike : bikes) {
            System.out.println(newBike);
        }
    }

}
